# Dendron Workspace Template for GitLab Pages Publishing

> These docs are under construction, and don't reflect GitLab Pages directions

<!--

![Logos for GitLab and Dendron](https://org-dendron-public-assets.s3.amazonaws.com/images/blog-header-dendron-netlify.png)

-->

- [View the Demo Website](https://dendronhq.gitlab.io/template.publish.gitlab)

This project is a minimal setup template for publishing a Dendron Workspace to GitLab Pages via GitLab CI.

Directions on how to use this template are mentioned in:
- #TODO

For directions on how to migrate and/or upgrade to the latest version of Dendron:
- [Dendron Publishing: Migrating and Upgrading](https://wiki.dendron.so/notes/rYbs1qLh9VJBXCJlSzMt4/)

Minimal directions:
- Copy this repository to a GitLab account
- Modify the `dendron.yml` with [new site information](https://gitlab.com/ScriptAutomate/template.publish.gitlab/-/blob/main/dendron.yml#L8-10) based on where the repository is going to exist
- GitLab CI should work out-of-box

## Other Publishing Paths

- [Dendron Docs: Publishing with GitHub Pages via GitHub Actions](https://wiki.dendron.so/notes/FnK2ws6w1uaS1YzBUY3BR/)
- [Share Your Notes Online: Publish Dendron with Netlify and GitHub](https://blog.dendron.so/notes/7h7zZkjF4Yqz8XSrHS1je)

## Related

* [Dendron Getting Started Guide](https://wiki.dendron.so/notes/678c77d9-ef2c-4537-97b5-64556d6337f1/)
* [Dendron Publishing Guide](https://wiki.dendron.so/notes/4ushYTDoX0TYQ1FDtGQSg/)
* [Dendron FAQ](https://wiki.dendron.so/notes/683740e3-70ce-4a47-a1f4-1f140e80b558/)
* [Dendron Concepts](https://wiki.dendron.so/notes/c6fd6bc4-7f75-4cbb-8f34-f7b99bfe2d50/)
* [GitLab Docs Home](https://docs.gitlab.com/ee/)
* [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
  * [GitLab Pages Access Control](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html)
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
  * [`.gitlab-ci.yml` configuration docs](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)

## Dendron Community

* Join [Dendron on Discord](https://link.dendron.so/discord)
* Follow [Dendron on Twitter](https://link.dendron.so/twitter)
* Follow [Dendron on Mastodon](https://link.dendron.so/mastodon)
* Join the [Dendron Newsletter](https://link.dendron.so/newsletter)
* Register for [Dendron Events on Luma](https://link.dendron.so/luma)
* Checkout [Dendron on GitHub](https://link.dendron.so/github)
* Read the [Dendron Blog](https://blog.dendron.so/)
* Visit and bookmark the [Dendron Smartpage](https://link.dendron.so/smartpage)
